import numpy as np
import sqlite3
import discord # For the User class
from discord.ext import commands
from ATLAS_utils import *
from datetime import datetime # To store when an item was reserved/sold

DATABASE_FILE = "discord_shop.db"
SHOP_NAME = "Animals N' Stuff"

class DiscordShopCog(commands.Cog, name = 'Discord Shop'):
    '''
    This cog is used to keep track of items/animals for sale in discord.
    You will be able to add/remove channels for what is currently for sale, and add/remove specific items for sale on each channel.
    Also, you will be able to keep track of who is taking care of each sale, and which sales have been completed.
    '''

    def __init__(self, bot, **kwargs):
        self.bot = bot
        self.guild = None

        # Create/load the database for anonymous ask/reply functions
        self.conn = sqlite3.connect(DATABASE_FILE)
        self.conn.row_factory = self.dict_factory
        self.c = self.conn.cursor()
        # saleState is 0 for purchasable, 1 for reserved, and 2 for sold.
        # Currently no use for 0 state, may include some functionality in the future.
        # Staff is the WE member that helped in the most recent interaction.
        self.c.execute("CREATE TABLE IF NOT EXISTS \
                SaleItems (id INTEGER PRIMARY KEY, \
                        name TEXT NOT NULL, \
                        saleState INTEGER DEFAULT 0, \
                        staff INTEGER NOT NULL DEFAULT -1, \
                        buyer INTEGER NOT NULL DEFAULT -1, \
                        date TEXT NOT NULL, \
                        UNIQUE(name));")
        self.conn.commit()

    def dict_factory(self, cursor, row):
        '''
        Used in sql tables to extract data into a dict.
        '''
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def _stringToMember(self, guild, memberString):
        '''
        Converts a username or mention into a member object.

        May be None if no user is found.
        '''
        if memberString == "None": return None
        if memberString[0:2] == '<@' and memberString[-1] == '>': # This is a mention
            memberString = memberString[2:-1] # Convert mention to discord ID
            if memberString[0] == '!': memberString = memberString[1:] # Remove extra '!' if user has a nick... ask discord.
            return guild.get_member(int(memberString))
        #Look for memberString as a display name, case-insensitively
        members = guild.members
        for member in members:
            if memberString.lower() == member.display_name.lower():
                return member
        return None
        #return guild.get_member_named(memberString)

    def _ConvertFieldToName(self, guild, field, items):
        '''
        Converts a field from discord IDs to discord names.
        '''
        for item in items:
            if item[field] == 0:
                item[field] = "Unknown"
            else:
                member = guild.get_member(item[field])
                if member is None:
                    item[field] = "Not in this server"
                else:
                    item[field] = member.display_name
        return items

    async def _PrintItems(self, ctx, items, header = [['Item', 'Reserved for', 'by staff', "on date"]]):
        '''
        Prints a list of items to discord.
        '''
        # Check if no items are for sale
        if not items:
            await PrintTo(ctx, 'No items match your search.')
            return

        mat = header + [[item['name'], item['buyer'], item['staff'], item['date']] for item in items]

        await PrintTo(ctx, GetMatPrintString(mat))
        return

    @commands.group()
    async def shop(self, ctx):
        '''
        Multi-purpose command to interact with our discord shop.
        '''
        if ctx.invoked_subcommand is None:
            await PrintTo(ctx, "A valid subcommand is needed.")
            await ctx.send_help(ctx.command)
        return

    @shop.command()
    @commands.has_role("White Elephant")
    async def buyer(self, ctx, item, newBuyer):
        '''
        Updates the buyer for an item, in case you forgot to include it or it changes.
        '''
        # Check for multiple buyer changes
        if ',' in item:
            itemList = item.split(',')
            for i in itemList:
                await self.buyer.callback(self, ctx, i, newBuyer)
            return
        newBuyer = self._stringToMember(ctx.guild, newBuyer)
        self.c.execute("UPDATE SaleItems SET buyer = ? WHERE upper(name) = ?", (newBuyer.id, item.upper()))
        self.conn.commit()
        await ctx.message.add_reaction(u"\u2705") # heavy white check mark
        return

    @shop.command()
    @commands.has_role("White Elephant")
    async def cancel(self, ctx, item):
        '''
        Cancels a reservation for an item in the shop.

        Will not remove any items that have been finalized,
        but will remove items that are reserved.
        '''
        self.c.execute("DELETE FROM SaleItems WHERE upper(name) = ? AND NOT saleState = 2", (item.upper(),))
        self.conn.commit()
        await ctx.message.add_reaction(u"\u2705") # heavy white check mark
        return

    @shop.command()
    async def history(self, ctx, filter = None):
        '''
        Prints out a history of sold items in the shop.
        Can also include a user to limit the results to items sold to them.
        '''
        buyer = self._stringToMember(ctx.guild, str(filter))

        if buyer is None:
            self.c.execute("SELECT name, buyer, staff, date FROM SaleItems WHERE saleState = 2")
        else:
            self.c.execute("SELECT name, buyer, staff, date FROM SaleItems WHERE buyer = ? AND saleState = 2", (buyer.id,))
            filter = None # Do not filter later on
        items = self.c.fetchall()

        # Filter out if requested
        if filter is not None:
            items = [item for item in items if filter in item['name']] # filter must be a substring in the name
        items = self._ConvertFieldToName(ctx.guild, 'buyer', items)
        items = self._ConvertFieldToName(ctx.guild, 'staff', items)

        # Filter out if requested
        if filter is not None:
            items = [item for item in items if filter in item['name']] # filter must be a substring in the name

        await self._PrintItems(ctx, items, header = [['Item', "Sold to", "by staff", "on date"]])
        return

    @shop.command()
    async def reservations(self, ctx, filter = None):
        '''
        Show a list of all currently reserved items.
        Can also include a user or item prefix to limit the results.
        '''
        buyer = self._stringToMember(ctx.guild, str(filter))

        if buyer is None:
            self.c.execute("SELECT name, buyer, staff, date FROM SaleItems WHERE saleState = 1")
        else:
            self.c.execute("SELECT name, buyer, staff, date FROM SaleItems WHERE buyer = ? AND saleState = 1", (buyer.id,))
            filter = None # Do not filter later on
        items = self.c.fetchall()
        items = self._ConvertFieldToName(ctx.guild, 'buyer', items)
        items = self._ConvertFieldToName(ctx.guild, 'staff', items)

        # Filter out if requested
        if filter is not None:
            items = [item for item in items if filter.upper() in item['name'].upper()] # filter must be a substring in the name

        await self._PrintItems(ctx, items)
        return

    @shop.command()
    @commands.has_role("White Elephant")
    async def reserve(self, ctx, item, buyer = None):
    #async def reserve(self, ctx, item, buyer: discord.Member = None):
        '''
        Used to mark an item in the shop as reserved by a discord user.
        '''
        # Check for multiple reservations
        if ',' in item:
            itemList = item.split(',')
            for i in itemList:
                await self.reserve.callback(self, ctx, i, buyer)
            return
        buyer = self._stringToMember(ctx.guild, str(buyer))

        # Check for a reserved item with the same name
        self.c.execute("SELECT name, buyer FROM SaleItems where saleState = 1")
        reserved = self.c.fetchall()
        reserved = self._ConvertFieldToName(ctx.guild, 'buyer', reserved)

        for resItem in reserved:
            print(item.upper(), resItem['name'].upper())
            if item.upper() == resItem['name'].upper():
                await PrintTo(ctx, "Sorry, this item has already been reserved for " + resItem['buyer'])
                return

        # Convert buyer mention into a discord ID
        if buyer is None:
            buyer = 0 # 0 is interpreted as "unknown buyer"
            await PrintTo(ctx, "Reserving for an undetermined buyer - use the 'buyer' command to indicate who the buyer is.")
        else:
            buyer = buyer.id # Convert discord.User into their discord id

        # Add an entry to the table with saleState = 1
        now = datetime.now()
        self.c.execute("INSERT INTO SaleItems (name, saleState, staff, buyer, date) VALUES (?, 1, ?, ?, ?)", \
                (item.upper(), ctx.message.author.id, buyer, now.strftime("%m/%d/%Y")))
        self.conn.commit()
        await ctx.message.add_reaction(u"\u2705") # heavy white check mark
        return

    @shop.command()
    @commands.has_role("White Elephant")
    async def sold(self, ctx, item):
        '''
        Mark an item as sold.
        '''
        # Check for multiple sales
        if ',' in item:
            itemList = item.split(',')
            for i in itemList:
                await self.sold.callback(self, ctx, i)
            return
        now = datetime.now()
        self.c.execute("UPDATE SaleItems SET saleState = 2, staff = ?, date = ? WHERE upper(name) = ?", (ctx.message.author.id, now.strftime("%m/%d/%Y"), item.upper()))
        self.conn.commit()
        await ctx.message.add_reaction(u"\u2705") # heavy white check mark
        return

    @buyer.error
    @cancel.error
    @reserve.error
    @sold.error
    async def on_command_error(self, ctx, error):
        await ctx.message.add_reaction(u"\u274C") # cross mark
        if isinstance(error, commands.CheckFailure):
            print(ctx.message.author.name + " tried to use a forbidden command.")
            await PrintTo(ctx, "Sorry, but only White Elephant members can use that command. Ask one for help!")
        else:
            raise error

def setup(bot):
    print("Loading Discord Shop Cog")
    bot.add_cog(DiscordShopCog(bot))
    print("Discord Shop Cog successfully loaded.")
