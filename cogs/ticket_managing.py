import sqlite3
from discord.ext import commands
from ATLAS_utils import *

DATABASE_FILE = "database.db"

class TicketManagerCog(commands.Cog, name = "Feedback/Questions"):
    '''
    Manages the collection and resolution of feedback or questions through tickets.
    '''
    def __init__(self, bot):
        self.bot = bot

        # Create/load the database for anonymous ask/reply functions
        self.conn = sqlite3.connect(DATABASE_FILE)
        self.conn.row_factory = self.dict_factory
        # conn.row_factory = dict_factory
        self.c = self.conn.cursor()
        self.c.execute("CREATE TABLE IF NOT EXISTS \
                   Ticket (id INTEGER PRIMARY KEY, \
                           discordId INTEGER NOT NULL, \
                           question TEXT NOT NULL, \
                           resolved INTEGER DEFAULT -1);")
        self.conn.commit()

    def createTicket(self, question, discordId):
        #c = conn.cursor()
        try:
            self.c.execute("INSERT INTO Ticket (discordId, question) VALUES (?, ?)", (str(discordId), question))
        except sqlite3.Error as e:
            print("Database error on creating ticket: %s" % e)
        self.conn.commit()
        return self.c.lastrowid

    def resolveTicket(self, ticketId):
        #c = conn.cursor()
        self.c.execute("UPDATE Ticket SET resolved = 1 WHERE id = ?", (ticketId,))
        self.conn.commit()
        return True

    def getTicket(self, ticketId):
        #c = conn.cursor()
        self.c.execute("SELECT id, discordId, question FROM Ticket WHERE id = ?", (ticketId,))
        row = self.c.fetchone()
        return row
        # return row if row is not None else None

    def getOpenTickets(self):
        #c = conn.cursor()
        self.c.execute("SELECT id, question FROM Ticket WHERE resolved = 0")
        return self.c.fetchall()

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    @commands.command()
    @commands.check(is_approved_responder)
    async def resolve(self, ctx, ticketId):
        '''
        Resolve a ticket. Usable only by the authorized users.
        '''
        resolved = self.resolveTicket(ticketId)
        ticket = self.getTicket(ticketId)
        if resolved:
            message = "Ticket " + str(ticket['id']) + " has been resolved."
            await PrintTo(ctx.message.author, message)

            message = "Your ticket has been marked as resolved. If you need further assistance, please open a new ticket.\nTicket ID: " + str(ticket['id']) + "\nQuestion:\n" + ticket['question']
            await PrintTo(self.bot.get_user(ticket['discordId']), message)
        else:
            message = "Ticket was unable to be resolved."
            await PrintTo(ctx.message.author, message)
        pass

    @commands.command()
    @commands.check(is_approved_responder)
    async def openTickets(self, ctx):
        '''
        Prints out a list of open questions (tickets). Usable only by authorized users.
        '''
        open = self.getOpenTickets()
        if not open:
            mat = ["There are no open tickets."]
        else:
            mat = [['Ticket ID', 'Question']] + [[ticket['id'], ticket['question']] for ticket in open]
        await PrintTo(ctx.message.author, GetMatPrintString(mat))

    @commands.command()
    async def reply(self, ctx, ticketId, *, reply):
        '''
        Reply to given ticket with a reply. Usable only be authorized users.
        '''
        ticket = self.getTicket(ticketId)
        if(ctx.message.author.id == ticket['discordId']):
            message = "__Your ticket has been updated__\nTicket ID: **" + ticketId + "**\n You will recieve an answer shortly."
            await PrintTo(ctx.message.author, message, raw=True)

            message = "__Ticket has been updated__\nTicket ID: **" + ticketId + "**\nReply:\n" + reply + "\n\nYou may respond by starting your message with:\n!reply " + ticketId
            destUser = self.bot.get_user(ASK_USER_ID)
            await PrintTo(destUser, message, raw=True)
        else:
            if not is_approved_responder(ctx):
                return
            message = "__Your ticket has been updated:__\nTicket ID = **" + ticketId + "**\n"
            message += "Your question: " + ticket['question']
            message += "\nAuthor response:\n" + reply
            message += "\n\nTo reply, enter your response that starts with:\n"
            message += "!reply " + str(ticketId)
            destUser = self.bot.get_user(ticket['discordId'])
            await PrintTo(destUser, message, raw=True)

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def ask(self, ctx, *, question):
        '''
        Ask the bot author a question, or get some clarification!
        The message will be deleted from the channel quickly, so your question will remain
        (almost certainly) confidential. The response will be from a DM.

        example: !ask Please add a command for me to ask you a question!
        '''
        print("Entering the ask function")
        # Delete the input question
        await ctx.message.channel.delete_messages([ctx.message])
        ticketId = self.createTicket(question, ctx.message.author.id)

        # Push the message through to the correct user
        message = "__New ticket:__\nTicket ID: **" + str(ticketId) + "**\nQuestion:\n" + question
        message += "\n\nTo reply, enter your response that starts with:\n"
        message += "!reply " + str(ticketId)
        destUser = self.bot.get_user(ASK_USER_ID)
        await PrintTo(destUser, message, raw=True)

        # Send a confirmation message
        message = "__Ticket created:__\nTicket ID = **" + str(ticketId) + "**\nYour question has been sent to the Bot Author. Expect a response in this channel.\n"
        message += "Message sent to author:\n" + question
        await PrintTo(ctx.message.author, message, raw=True)

    @ask.error
    async def ask_error(self, ctx, error):
        if isinstance(error, commands.CheckFailure):
            await PrintTo(ctx, "This bot does not have permission to delete messages - add this permission to use the ask function.")
        if isinstance(error, commands.CommandInvokeError):
            await PrintTo(ctx.message.author, "Welp, that didn't work. Ask again, but better this time.")

def setup(bot):
    print("Loading Ticket Manager Cog")
    bot.add_cog(TicketManagerCog(bot))
    print("Ticket Manager Cog successfully loaded.")
