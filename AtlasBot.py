import discord, random, gspread, itertools, colorsys, json
import numpy as np
from discord.ext import commands
from oauth2client.service_account import ServiceAccountCredentials
import matplotlib.pyplot as plt
import matplotlib.colors
import os
from dotenv import load_dotenv
from ATLAS_utils import *

load_dotenv() # Save variables in the .env file to environment variables

# Gain access using credentials
SCOPE=['https://www.googleapis.com/auth/drive']
CREDS = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', SCOPE)

PREFIX=("!","") # Match no prefix, or ! prefix
CLOSE_FILE_PATH = "closing_lines.txt"
DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
COGS_DIR = "cogs"

bot = commands.Bot(command_prefix=PREFIX, description="White Elephant Atlas Bot: Here for all of your ATLAS needs!", case_insensitive=True)

def ExtractAnimalData(breed = None):
    '''
    Extracts all animal information from the spreadsheet, for the given breed.

    If no breed is listed, will print out a list of all animals recorded.
    '''
    gs = gspread.authorize(CREDS)
    animalData = gs.open("White Elephant Data Portal (Responses)").worksheet("Animal_data").get_all_values()
    headers = animalData.pop(0)

    # Just pull out animals of the specified breed
    if breed is not None: # extract all animals that have been recorded
        animalData = [ animal for animal in animalData if breed.lower()==animal[0].lower()]

    animalData.insert(0, headers)
    return np.asarray(animalData)

def ExtractIslandData(resource = None):
    '''
    Extracts all islands from the island spreadsheet that contain the given resource.
    '''
    gs = gspread.authorize(CREDS)
    islandData = gs.open("White Elephant Data Portal (Responses)").worksheet("Island_data").get_all_values()
    headers = islandData.pop(0)

    # Just pull out islands that contain your desired resource
    if resource is not None: # extract all islands that have been recorded
        islandData = [ island for island in islandData if any(resource.lower() in x.lower() for x in island)]
    # To just get full-word matches, need to check against a list of words instead of a string (ex. 'salt' shouldn't match 'basalt')

    islandData.insert(0, headers)
    return np.asarray(islandData)

@bot.event
async def on_ready():
    print("The bot is ready!")
    await bot.change_presence(activity=discord.Activity(name='you play ATLAS', type=discord.ActivityType.watching))


@bot.command()
async def animals(ctx, breed = None):
    '''
    Lists all animals in the data portal of a given breed.

    If no breed is listed, will list all animals in the data portal, sorted by breed.
    '''

    animals = ExtractAnimalData(breed)
    headers, animals = animals[0], animals[1:]
    animals.sort(axis=0)

    if breed is None:
        message = "Recorded animals:\n"
    else:
        message = "Here are all of our " + breed + "s\n"
        animals = animals[:,1:] # get rid of breed column
        headers = headers[1:]

    animals = np.insert(animals, 0, headers, 0)
    message += GetMatPrintString(animals)
    await PrintTo(ctx, message)

class Stat():
    def __init__(self, name):
        self.name = name
        self.base = None # Value if no levels have been put in
        self.wildP = None # % of base to be added per wild level
        self.tameP = None # % of base to be added per tame level
        self.constant = None # Constant to convert round-up to regular rounding, or just fit data better
        self.imprintConst = None # For Melee, subtracts some from the stat, based on imprint percentages - probably an error in the form of my fit function.
        self.tolMult = None # Controls tolerance for each stat, to incorporate certainty of the fit
        self.affectedBy = None # Dictates whether Nothing, imprinting, or imprinting and taming prof. affect it
        self.hidden = None # Dictates whether the stat is actually shown in the GUI

    def loadJson(self, jsonDict):
        if 'base' in jsonDict: self.base = jsonDict['base']
        if 'wildP' in jsonDict: self.wildP = jsonDict['wildP']
        if 'tameP' in jsonDict: self.tameP = jsonDict['tameP']
        if 'constant' in jsonDict: self.constant = jsonDict['constant']
        if 'imprintConst' in jsonDict: self.imprintConst = jsonDict['imprintConst']
        if 'tolMult' in jsonDict: self.tolMult = jsonDict['tolMult']
        if 'affectedBy' in jsonDict: self.affectedBy = jsonDict['affectedBy']
        if 'hidden' in jsonDict: self.hidden = jsonDict['hidden']
        return self

    async def ComputeLevels(self, ctx, stat_value, wild_levels, tame_levels, imprintRange = None, tol = 0.1):
        '''
        Returns a list of dictionaries, with number of Wild/Tame levels, imprint Percent, and tame prof.
        '''
        # Create a list of all possible combinations of Wild and Tamed levels
        levelConfigs = np.array([ [W, T] for W in range(wild_levels+1) for T in range(tame_levels+1) ])

        possLevels = []
        stat_value -= self.constant # Remove any constant term
        tol *= self.tolMult # Dictates our certainty in our fit parameters
        #if self.name == "Health": stat_value -= 0.05 # Convert round-up rule to regular rounding
        #if self.name == "Melee": stat_value -= 0.065 # Fit from my own data.... odd addition
        for W, T in levelConfigs:
            prediction = self.base*(1 + W * self.wildP) * (1 + T * self.tameP)
            if self.affectedBy == "Nothing":
                if abs(prediction - stat_value) < tol: # Give margin of error to help with rounding error
                    possLevels.append({"wild":W, "tame":T})
            if self.affectedBy == "Imprinting": # We need to match up imprinting percentages
                imprintP_min = ((stat_value-0.05)/(prediction+tol) - 1)/0.2
                imprintP_max = ((stat_value+0.05)/(prediction-tol) - 1)/0.2
                if 0 <= imprintP_max and imprintP_min <= 1:
                    possLevels.append({"wild":W, "tame":T, "imprint_min":imprintP_min, "imprint_max": imprintP_max})
            if self.affectedBy == "Proficiency": # Now we need tame prof. and imprint % (already given, hopefully)
                tameP_min = (stat_value+self.imprintConst*imprintRange[0]-0.05)/((prediction+tol)*(1+0.2*imprintRange[1]))
                tameP_max = (stat_value+self.imprintConst*imprintRange[1]+0.05)/((prediction-tol)*(1+0.2*imprintRange[0]))
                print("proficiency:", tameP_min,tameP_max, ":",imprintRange[0], imprintRange[1], ":", W, T)
                if 0 <= tameP_max and tameP_min <= 1:
                    possLevels.append({"wild":W, "tame":T, "prof_min":tameP_min, "prof_max": tameP_max})

        if not possLevels and self.name != "Melee": # Melee is ok to find nothing - it's checked directly against the rest of the levels
            await PrintTo(ctx, "Could not find a valid level configuration for the " + self.name + " stat you entered.")

        return possLevels

def Intersect(*intervals, start = [0,1]):
    '''
    Returns the intersection of the given intervals and [0,1] (by default)
    '''
    cap = start # Control valid intervals to a default range
    for interval in intervals:
        if cap[0] > interval[1] or cap[1] < interval[0]:
            return None # Do not intersect
        cap = [max(cap[0], interval[0]), min(cap[1], interval[1])]
    return cap

@bot.command()
async def animalstats(ctx, breed, wild_levels, tame_levels, *, stats):
    '''
    Determine the number of wild and tamed levels in each stat, given a breed and list of stats.

    The stats are ordered top to bottom, left column then right column.
    For most animals, this is health, food, oxygen, weight, stamina, and then melee.

    TIP: If you get multiple configurations from this command, add a level and try again. This will narrow down
    the options, and you can keep doing this until only 1 possibility remains. On the bright side, you only need
    1 generation to figure it out, instead of many!
    '''
    statVals = None
    breed = breed.lower()
    with open('animal_stats.json', 'r') as json_file:
        statVals=json.load(json_file)

    if statVals == None:
        await PrintTo(ctx, "Could not load the animal stat database. DM Simon to fix this...")
        exit(-1)

    if breed not in statVals:
        breedNames = [*statVals] # equivalent to list(statVals), but about 2x faster
        breedNames[-1] = "and " + breedNames[-1]
        await PrintTo(ctx, "We only have stats for " + ', '.join(breedNames) + " for now. Sowwy!")
        return

    # Compare that to just a list of stats
    # Stats are ordered by their appearance in the "default" stat block
    statParams = []
    for name, stat in statVals['default'].items():
        # Load default parameters
        statParams.append(Stat(name).loadJson(stat))
        # Overwrite with breed-specific get_all_values
        if statParams[-1].name in statVals[breed]:
            statParams[-1].loadJson(statVals[breed][statParams[-1].name])
        # And finally, remove this stat if it is hidden
        if statParams[-1].hidden: del statParams[-1]

    # Start by converting input strings to numbers
    try:
        wild_levels = int(wild_levels) - 1 # Remove single base stat
        tame_levels = int(tame_levels)
    except ValueError as e:
        await PrintTo(ctx, "Please remember to include wild and tame levels after the breed name.")
        return
    stats = np.array(stats.split()).astype(np.float)

    # Stage 1.0: Compute Wild/Tamed level configurations for stats 'affectedBy' nothing
    regLevels = []
    for idx,stat in enumerate(statParams):
        if(stat.affectedBy == 'Nothing'): # Simply compute levels - should be easy
            regLevels.append(await stat.ComputeLevels(ctx, stats[idx], wild_levels, tame_levels))
        currentConfigs = list(itertools.product(*regLevels))

    # Stage 1.1: Remove configurations where the wild/tame levels exceed total wild/tame Levels
    for config in currentConfigs.copy():
        WLused = np.sum([stat['wild'] for stat in config])
        TLused = np.sum([stat['tame'] for stat in config])
        if WLused > wild_levels or TLused > tame_levels:
            currentConfigs.remove(config)

    # Stage 1.2: See if any configurations remain.
    if len(currentConfigs) == 0:
        await PrintTo (ctx, "No valid configurations found - make sure you are including all decimals.")
        return

    # Stage 2.0: Like step 1.0, but now we need to unify multiple stats with their imprinting percentage
    imprintLevels = []
    WLRemaining = wild_levels - np.min([np.sum([stat['wild'] for stat in config]) for config in currentConfigs])
    TLRemaining = tame_levels - np.min([np.sum([stat['tame'] for stat in config]) for config in currentConfigs])

    print("After regular configs, have", WLRemaining, "wild levels and", TLRemaining, "tame levels remaining")
    print("And there are", len(currentConfigs), "configurations")
    print([stat.name for stat in statParams if stat.affectedBy == "Nothing"])
    print(currentConfigs)
    for idx,stat in enumerate(statParams):
        if(stat.affectedBy == 'Imprinting'):
            imprintLevels.append(await stat.ComputeLevels(ctx, stats[idx], WLRemaining, TLRemaining))

    # Compute the configurations - there are probably a lot, so check validity after adding each stat to reduce combinations
    for idx,statLevels in enumerate(imprintLevels):
        if idx == 0:
            imprintConfigs = [[config] for config in statLevels]
        else:
            imprintConfigs = list(itertools.product(imprintConfigs, [[config] for config in statLevels]))
            imprintConfigs = [list(np.concatenate((config[0], config[1]))) for config in imprintConfigs]
        print(idx, "th imprint stat starts at", len(imprintConfigs), "configs")

        # Stage 2.1: Remove configurations where the wild/tame levels exceed remaining wild/tame Levels
        # OR where the imprint percentages don't overlap
        for config in imprintConfigs.copy():
            #print(config)
            WLused = np.sum([stat['wild'] for stat in config])
            TLused = np.sum([stat['tame'] for stat in config])
            imprintRange = Intersect(*[[stat['imprint_min'], stat['imprint_max']] for stat in config])
            if WLused > WLRemaining or TLused > TLRemaining or imprintRange is None:
                imprintConfigs.remove(config)

    # Stage 2.2: See if any configurations remain.
    if len(imprintConfigs) == 0:
        await PrintTo (ctx, "No valid configurations found (no valid imprint percentage determined) - make sure you are including all decimals.")
        return

    # Stage 2.3: Combine with previous configurations, and check again for levels like 1.1
    currentConfigs = list(itertools.product(currentConfigs, imprintConfigs))
    currentConfigs = [list(np.concatenate((config[0], config[1]))) for config in currentConfigs]
    for config in currentConfigs.copy():
        WLused = np.sum([stat['wild'] for stat in config])
        TLused = np.sum([stat['tame'] for stat in config])
        if WLused > wild_levels or TLused > tame_levels:
            currentConfigs.remove(config)
    print("and", len(imprintConfigs), "final imprint configurations")

    # Stage 3.0: Same again, but now we have an imprint % for each config to now compute tame %
    finalConfigs = []
    print("after regular+imprint configs, have only", WLRemaining, "wild levels and", TLRemaining, "tame levels remaining")
    print("and there are", len(currentConfigs), "configurations")
    print(currentConfigs)
    for config in currentConfigs:
        WLRemaining = wild_levels - np.sum([stat['wild'] for stat in config])
        TLRemaining = tame_levels - np.sum([stat['tame'] for stat in config])
        imprintRange = Intersect(*[[stat['imprint_min'], stat['imprint_max']] for stat in config if 'imprint_min' in stat and 'imprint_max' in stat])
        profLevels = [] # Do a first pass to exclude any configs with a bad combo of proficiency
        for idx,stat in enumerate(statParams):
            if(stat.affectedBy == 'Proficiency'):
                #profLevels.append(await stat.ComputeLevels(ctx, stats[idx], maxWLRemaining, maxTLRemaining, imprintRange = imprintRange))
                profLevels.append(await stat.ComputeLevels(ctx, stats[idx], WLRemaining, TLRemaining, imprintRange = imprintRange))

        # This next section currently has no purpose, since only Melee (I think) is affected by proficiency
        # Longevity/Robustness!
        # Stage 3.1: Remove configurations where the wild/tame levels exceed remaining wild/tame Levels
        # OR where the imprint percentages don't overlap
        profConfigs = list(itertools.product(*profLevels))
        for profConfig in profConfigs.copy():
            WLused = np.sum([stat['wild'] for stat in profConfig])
            TLused = np.sum([stat['tame'] for stat in profConfig])
            profRange = Intersect(*[[stat['prof_min'], stat['prof_max']] for stat in profConfig])
            if WLused > WLRemaining or TLused > TLRemaining or profRange is None:
                profConfigs.remove(profConfig)

        # Stage 3.2: See if any configurations remain.
        #if len(profConfigs) == 0:
        #    await PrintTo (ctx, "No valid configurations found (no valid tame proficiency) - make sure you are including all decimals.")
        #    continue
            #return

        # Stage 3.3: Combine with previous configurations, and check again for levels like 1.1
        tempConfigs = list(itertools.product([config], profConfigs))
        tempConfigs = [list(np.concatenate((config[0], config[1]))) for config in tempConfigs]
        for config in tempConfigs.copy():
            WLused = np.sum([stat['wild'] for stat in config])
            TLused = np.sum([stat['tame'] for stat in config])
            if WLused > wild_levels or TLused > tame_levels:
                tempConfigs.remove(config)
        for config in tempConfigs:
            finalConfigs.append(config)
        print("which leaves", len(finalConfigs), "final proficiency configurations")

    # Stage 3.4: See if any configurations remain.
    if len(finalConfigs) == 0:
        await PrintTo (ctx, "No valid configurations found (no valid tame proficiency) - make sure you are including all decimals, especially for melee. When in doubt, remove a point or two from melee (melee equation is odd, if you couldn't tell :P).")
        return

    # Stage 4: Remove configurations that don't add up to the corrent number of levels
    # Also remove configurations with proficiency<100% and imprint>0% (since bred animals always have 100% prof.)
    keepConfig = np.ones(len(finalConfigs), dtype = np.int8)
    for index,config in enumerate(finalConfigs):
        numWildLevels = np.sum([ stat['wild'] for stat in config])
        numTameLevels = np.sum([ stat['tame'] for stat in config])
        if numWildLevels != wild_levels or numTameLevels != tame_levels:
            keepConfig[index] = 0 # Remove this one
        profMax = Intersect(*[[stat['prof_min'], stat['prof_max']] for stat in config if 'prof_min' in stat and 'prof_max' in stat])[1]
        imprintMin = Intersect(*[[stat['imprint_min'], stat['imprint_max']] for stat in config if 'imprint_min' in stat and 'imprint_max' in stat])[0]
        if profMax < 1 and imprintMin > 0:
            keepConfig[index] = 0 # Remove this configuration
    finalConfigs = np.asarray(finalConfigs)[keepConfig == 1]

    # Stage 4.5: Check to see if any configurations remain.
    if len(finalConfigs) == 0:
        await PrintTo(ctx, "No valid configurations found. No configurations added up to the correct number of levels, or there was an issue with matching taming proficiency with imprint percentage.")
        return

    # Stage 5: Clean up the final configurations and display results
    prof = [Intersect(*[[stat['prof_min'], stat['prof_max']] for stat in config if 'prof_min' in stat and 'prof_max' in stat])[1] for config in finalConfigs]
    finalConfigs = np.asarray(finalConfigs)[np.argsort(prof)[::-1]] # Sort by highest tame proficiency, since it is usually right around 100%
    numConfigs = min(len(finalConfigs), 5)
    message = ""
    if numConfigs < len(finalConfigs):
        message += "Some configurations have been ignored, and the top 5 choices (out of " + str(len(finalConfigs)) +") are displayed\n"
        finalConfigs = finalConfigs[:numConfigs]
    #Reorder configs to align with the order they were input and are stored in the JSON files
    configOrder = []
    configOrder += [stat.name for stat in statParams if stat.affectedBy == "Nothing"]
    configOrder += [stat.name for stat in statParams if stat.affectedBy == "Imprinting"]
    configOrder += [stat.name for stat in statParams if stat.affectedBy == "Proficiency"]
    print("Stats determined in the order:", configOrder)
    reorder = [ configOrder.index(stat.name) for stat in statParams]
    print("will become:", np.asarray(configOrder)[reorder])
    finalConfigs = np.asarray(finalConfigs)[:,reorder]

    message += "Possible level configurations:\n"
    data = ["", "Base"] + [stat.name for stat in statParams]#statNames
    # Make mini-matrices
    for idx, config in enumerate(finalConfigs):
        imprintRange = Intersect(*[[stat['imprint_min'], stat['imprint_max']] for stat in config if 'imprint_min' in stat and 'imprint_max' in stat])
        profRange = Intersect(*[[stat['prof_min'], stat['prof_max']] for stat in config if 'prof_min' in stat and 'prof_max' in stat])
        imprintRange = [round(100*elem, 2) for elem in imprintRange]
        profRange = [round(100*elem, 2) for elem in profRange]
        wildLevels = ["Wild Levels"] + [1] + [stat['wild'] for stat in config]
        tameLevels = ["Tame Levels"] + [0] + [stat['tame'] for stat in config]

        message += "Taming Prof.: " + str(profRange[0]) + "-" + str(profRange[1]) + "%,\t"
        message += "Imprint:" + str(imprintRange[0]) + "-" + str(imprintRange[1]) + "%\n"
        message += GetMatPrintString([data, wildLevels, tameLevels]) + "\n"

    await PrintTo(ctx, message)

@bot.command()
async def whatsat(ctx, coord = None):
    '''
    Tells you what resources can be found in a specific grid location.
    '''
    islandData = ExtractIslandData()

    data = []

    if coord is None:
        # Extract a list of all grids with recorded islands
        islandData = islandData[1:] # ignore the headers
        gridList = islandData[:,0].flatten()
        # Remove duplicates and empty elements, then sort the list
        gridList = np.asarray(list(filter(None, gridList)))
        gridList = np.unique(gridList)
        gridList = np.asarray(sorted(gridList, key=CoordToID))

        message = "Islands have been recorded in the following grids:\n"
        message += GetMatPrintString(gridList)
        await PrintTo(ctx, message)

    headers, islandData = islandData[0], islandData[1:]
    #Extract data for the requested grid
    for row in islandData:
        if row[0].lower() == coord.lower():
            data.append(row)

    if(data == []):
        await PrintTo(ctx, "We have no data for resources in grid " + coord)
        return

    # Remove empty columns, then sort by island name
    data = np.asarray(data)
    keepCols = (data!="").any(axis=0)
    keepCols[0] = False # Remove the first column (Grid)
    data = data[:,keepCols]
    headers = headers[keepCols]
    data.sort(axis=0)

    data = np.insert(data, 0, headers, axis=0)

    message = "Resources in " + coord.upper() + ":\n"
    message += GetMatPrintString(data)

    await PrintTo(ctx, message)

@bot.command()
async def whereis(ctx, *, resource = None):
    '''
    Figure out where a specific resource can be found.
    If you specify a resource, it will print out a list of islands where it can be found.
    If you dont specify a resource, it will list all known resources.
    '''
    islandData = ExtractIslandData(resource)
    if len(islandData) == 1: # Just has a header, and no data
        await PrintTo(ctx, "We have no data on where to find " + resource)
        return

    if resource == None:
        # Extract a list of all recorded resources
        islandData = islandData[1:] # ignore the headers
        resourceList = np.array([ [x for x in island] for island in np.array(islandData)[:,5:27]]).flatten()
        resourceList = np.hstack( [ x.split(", ") for x in resourceList ])
        # Remove duplicates and empty elements
        resourceList = np.asarray(list(filter(None, resourceList)))
        resourceList = np.unique(resourceList)
        resourceList.sort()

        message = "All currently recorded resources:\n"
        message += GetMatPrintString(resourceList)
        await PrintTo(ctx, message[:-2])
        return

    islandData = islandData[:,0:4] # Just keep the info we want
    print(islandData)

    message = "The resource " + resource + " can be found at:\n" + GetMatPrintString(islandData)
    await PrintTo(ctx, message)

@bot.command()
async def mapof(ctx, *, resource = None):
    '''
    Will plot a grid, showing where on the ATLAS map you can find a given resource.
    '''
    locations = ExtractIslandData(resource)[1:,0] # Just pull out what grids have the resource in them, ignoring the header

    hasResource = np.zeros((15,15))
    if resource is None: # Count up the number of islands in each grid
        locations = np.array([ x.lower() for x in locations])
        for grid in locations:
            x = ord(grid[0])-ord('a')
            y = int(grid[1:])-1
            hasResource[x,y] += 1
    else: # Just tally if it is there or not
        locations = np.unique([ x.lower() for x in locations])
        for grid in locations:
            x = ord(grid[0])-ord('a')
            y = int(grid[1:])-1
            hasResource[x,y] = 1

    # Start putting the plot together
    fig, ax = plt.subplots(1,1)
    if resource is None:
        # Make our own colormap
        hsv = [[h, 0.5, 0.6] for h in np.linspace(0, 1, int(np.max(hasResource))+1)]
        colors = [list(colorsys.hls_to_rgb(h[0], h[1], h[2])) for h in hsv]
        colors[0] = [1, 1, 1] # Make the '0' color white
        cmap = matplotlib.colors.ListedColormap(colors)
        plt.matshow(hasResource.T, cmap=cmap, vmin=-0.5, vmax=np.max(hasResource)+0.5)
        plt.title("Number of islands recorded in each grid", y = 1.08)
        plt.colorbar(ticks=np.arange(0,np.max(hasResource)+1))
    else:
        cmap = plt.get_cmap('Greens')
        plt.matshow(hasResource.T, cmap='Greens')
        plt.title("Where to find " + resource, y = 1.08)

    # Change axes to look good, and add horiz./vert. lines
    ticks=np.arange(15)
    plt.xticks(ticks, [chr(x+65) for x in ticks])
    plt.yticks(ticks,ticks+1)
    ax.xaxis.tick_top()
    plt.hlines(ticks-0.5,-0.5,14.5)
    plt.vlines(ticks+0.5,-0.5,14.5)

    plt.savefig("map.png")
    file = discord.File("map.png", filename="map.png")
    await ctx.send("", file=file)

@bot.command()
@commands.is_owner()
async def stop(ctx):
    '''
    Kills the robot. This command is only usable by Admirals.
    Don't you dare try to use this if you aren't a member of that role!
    Please?

    NOTE: Cannot be started again without getting Simon to start it up again.
    Good luck.

    example: !stop
    '''
    print("trying to stop the bot")
    with open(CLOSE_FILE_PATH, "r") as f:
        lines = f.readlines()
        print("Got a death message picked out")
    randLine = random.randint(0,len(lines)-1)
    print("Printing the death message,")
    print(randLine)
    print(lines[randLine])
    await PrintTo(ctx, lines[randLine])
    print("Logging out")
    await bot.logout()

@stop.error
async def stop_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        print("Nice try, bucko! User " + ctx.message.author.name + " unsuccessfully tried to kill me!")
        await PrintTo(ctx, "Sorry, but you _*dont*_ have my permission to kill me!")

@bot.command()
@commands.is_owner()
async def load(ctx, *, module : str):
    """Loads a module."""
    module = COGS_DIR + "." + module # Direct the module to the correct directory
    try:
        bot.load_extension(module)
    except Exception as e:
        await PrintTo(ctx,'\N{PISTOL}')
        await PrintTo(ctx,'{}: {}'.format(type(e).__name__, e))
    else:
        await ctx.message.add_reaction(u"\U0001F44C") # ok hand sign

@bot.command()
@commands.is_owner()
async def unload(ctx, *, module : str):
    """Unloads a module."""
    module = COGS_DIR + "." + module # Direct the module to the correct directory
    try:
        bot.unload_extension(module)
    except Exception as e:
        await PrintTo(ctx,'\N{PISTOL}')
        await PrintTo(ctx,'{}: {}'.format(type(e).__name__, e))
    else:
        await ctx.message.add_reaction(u"\U0001F44C") # ok hand sign

@bot.command()
@commands.is_owner()
async def reload(ctx, *, module : str):
    """Reloads a module."""
    module = COGS_DIR + "." + module # Direct the module to the correct directory
    try:
        bot.unload_extension(module)
        bot.load_extension(module)
    except Exception as e:
        await ctx.message.add_reaction(u"\U0001F52B") # pistol sign
        await PrintTo(ctx,'{}: {}'.format(type(e).__name__, e))
    else:
        await ctx.message.add_reaction(u"\U0001F44C") # ok hand sign

print("****************************************")
print("Starting the bot")
print("****************************************")

for filename in os.listdir(COGS_DIR):
    if filename.endswith(".py"):
        cogpath = COGS_DIR + "." + filename.replace(".py", "")
        bot.load_extension(cogpath)

bot.run(DISCORD_BOT_TOKEN)
